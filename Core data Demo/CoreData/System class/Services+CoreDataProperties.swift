//
//  Services+CoreDataProperties.swift
//  Core data Demo
//
//  Created by Raj Gajera on 22/09/19.
//  Copyright © 2019 Raj Gajera. All rights reserved.
//
//

import Foundation
import CoreData


extension Services {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Services> {
        return NSFetchRequest<Services>(entityName: "Services")
    }

    @NSManaged public var serviceId: Int32
    @NSManaged public var serviceName: String?
    @NSManaged public var service: Vehicle?

}
