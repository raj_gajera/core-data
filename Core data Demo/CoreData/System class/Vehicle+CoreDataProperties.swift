//
//  Vehicle+CoreDataProperties.swift
//  Core data Demo
//
//  Created by Raj Gajera on 22/09/19.
//  Copyright © 2019 Raj Gajera. All rights reserved.
//
//

import Foundation
import CoreData


extension Vehicle {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Vehicle> {
        return NSFetchRequest<Vehicle>(entityName: "Vehicle")
    }

    @NSManaged public var vehicleTypeId: Int32
    @NSManaged public var vehicleName: String?
    @NSManaged public var vehicleImage: String?
    @NSManaged public var service: NSSet?

}

// MARK: Generated accessors for service
extension Vehicle {

    @objc(addServiceObject:)
    @NSManaged public func addToService(_ value: Services)

    @objc(removeServiceObject:)
    @NSManaged public func removeFromService(_ value: Services)

    @objc(addService:)
    @NSManaged public func addToService(_ values: NSSet)

    @objc(removeService:)
    @NSManaged public func removeFromService(_ values: NSSet)

}
