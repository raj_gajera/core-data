//
//  Services+CoreDataClass.swift
//  Core data Demo
//
//  Created by Raj Gajera on 22/09/19.
//  Copyright © 2019 Raj Gajera. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(Services)
public class Services: NSManagedObject {

    class func createService (dict : [String:Any]) -> Services{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let service = Services(context:appDelegate.persistentContainer.viewContext)
        
        if let serviceId = dict["serviceId"] as? Int{
            service.serviceId = Int32(serviceId)
        }
        if let serviceName = dict["serviceName"] as? String{
            service.serviceName = serviceName
        }
        
        return service
        
    }
    
}
