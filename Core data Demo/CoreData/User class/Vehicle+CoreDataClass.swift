//
//  Vehicle+CoreDataClass.swift
//  Core data Demo
//
//  Created by Raj Gajera on 22/09/19.
//  Copyright © 2019 Raj Gajera. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(Vehicle)
public class Vehicle: NSManagedObject {

    class func insertRecords (arrRecords : [[String:Any]]) -> [Vehicle]{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let array = arrRecords.compactMap { (dict) -> Vehicle? in
            
            let vehicleObject = Vehicle(context: appDelegate.persistentContainer.viewContext)
            
            if let vehicleTypeId = dict["vehicleTypeId"] as? Int{
                vehicleObject.vehicleTypeId = Int32(vehicleTypeId)
            }
            if let vehicleTypeName = dict["vehicleName"] as? String{
                vehicleObject.vehicleName = vehicleTypeName
            }
            if let vehicleImage = dict["vehicleImage"] as? String{
                vehicleObject.vehicleImage = vehicleImage
            }
            
            if let serviceObjects = dict["Services"] as? [[String:Any]]{
                let _ = serviceObjects.compactMap({ (dictObject) in
                    vehicleObject.addToService(Services.createService(dict: dictObject))
                })
            }
            appDelegate.saveContext()
            return vehicleObject
        }
        
        return array
        
    }
    
    class func getAllRecord () -> [Vehicle]{
        
        do{
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let request : NSFetchRequest<Vehicle> = Vehicle.fetchRequest()
            let results = try appDelegate.persistentContainer.viewContext.fetch(request)
            return results
        }catch{
            
        }
        return []
        
    }
    
}
