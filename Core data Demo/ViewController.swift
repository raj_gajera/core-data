//
//  ViewController.swift
//  Core data Demo
//
//  Created by Raj Gajera on 22/09/19.
//  Copyright © 2019 Raj Gajera. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var arrVehicles : [Vehicle] = []
    @IBOutlet weak var tableView: UITableView!
    var closureName: (([[String: Any]]) -> [[String: Any]])?
    var arr = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrVehicles = Vehicle.getAllRecord()
        
        if arrVehicles.count == 0 {
            self.apiCall()
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func apiCall() {
        let headers = [
            "Cache-Control": "no-cache",
            "Postman-Token": "b9a122c1-5d62-ac21-6458-7c6aea983681"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.myjson.com/bins/19pfzd")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let data = data{
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]{
                        print(json)
                        
                        if let arrData = json["data"] as? [[String:Any]] {
                            self.arr = arrData
                            
                            DispatchQueue.main.async {
                                self.arrVehicles = Vehicle.insertRecords(arrRecords: arrData)
                                self.tableView.reloadData()
                            }
                        }
                        
                    }
                    
                }catch{
                    
                }
            }
        })
        
        dataTask.resume()
    }
}

extension ViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVehicles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        
        let object = arrVehicles[indexPath.row]
        
        cell.textLabel?.text = object.vehicleName ?? ""
        cell.detailTextLabel?.text = object.vehicleImage ?? ""
        
        return cell
    }
    
}

extension ViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
         let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDetailViewController") as! ServiceDetailViewController
        
            controller.vehicleObject = arrVehicles[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
    }
    
}


