//
//  ServiceDetailViewController.swift
//  Core data Demo
//
//  Created by Raj Gajera on 22/09/19.
//  Copyright © 2019 Raj Gajera. All rights reserved.
//

import UIKit

class ServiceDetailViewController: UIViewController {
    var vehicleObject : Vehicle?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionADD(_ sender: Any) {
        let objectService = Services.createService(dict: ["serviceName":"Tester service","serviceId":000])
        vehicleObject?.addToService(objectService)
        appDelegate.saveContext()
        self.tblView.reloadData()
    }

    @IBAction func actionTrash(_ sender: Any) {
        
        if let services = vehicleObject?.service, let serviceObject = services.allObjects.last as? Services{
            vehicleObject?.removeFromService(serviceObject)
            appDelegate.saveContext()
            self.tblView.reloadData()
        }
        
    }
    
    
       
}

extension ServiceDetailViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleObject?.service?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell1")
        
        if let service = vehicleObject?.service,let serviceObject = service.allObjects[indexPath.row] as? Services{
            cell.textLabel?.text = serviceObject.serviceName ?? ""
            cell.detailTextLabel?.text = "\(serviceObject.serviceId)"
        }
        return cell
        
    }
    
}

extension ServiceDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
